import { Component, OnInit, Input } from '@angular/core';
import { PersonService } from '../person.service';
import { Person, Relation } from '../model/Person';
import { ActivatedRoute, Router } from '@angular/router';
import { RelationTexts } from '../model/RelationTexts';
import * as _ from 'lodash';

@Component({
  selector: 'app-kontakt-details',
  templateUrl: './kontakt-details.component.html',
  styleUrls: ['./kontakt-details.component.scss']
})
export class KontaktDetailsComponent implements OnInit {
  public person: Person;
  public relationTexts = RelationTexts;

  public showAddRelationsDialog: boolean = false;
  personToDelete: number = null;
  relationToDelete: number = null;
  errors: Array<string> = [];
  

  constructor(private personService: PersonService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.refreshPerson();
  }


  /**
   * Aktualisiert die Ansicht der angezeigten Person
   */
  refreshPerson() {
    const id = (this.route.snapshot.paramMap.get('id'));
    if (id == "neu") {
      this.person = new Person();
    } else {
      this.personService.fetchPerson(parseInt(id)).then(p => {
        this.person = p;
      })
    }
  }

  /**
   * Entscheidet, ob eine Person aktualisiert oder neu angelegt wird (basierend ob eine ID vorhanden ist)
   */
  save() {
    if (this.person.id == null) {
      this.personService.create(this.person).then((resp: Response) => this.handleSaveResponse(resp));
    } else {
      this.personService.save(this.person).then((resp: Response) => this.handleSaveResponse(resp));
    }
  }


  /**
   * Überprüft, ob eine Response fehlerhaft ist und leitet diese dann an showError() weiter
   * @param resp Zu überprüfende Response
   */
  handleSaveResponse(resp: Response) {
    if (!resp.ok) {
      this.showError(resp)
    } else {
      this.navigateToOverview()
    }
  }

  /**
   * Zeigt Fehler einer Response an
   * @param resp Response die Fehler beeinhaltet
   */
  showError(resp: Response) {
    if(resp.status === 412){
      this.errors = ["Es wurden in der Zwischenzeit Änderungen an dem Kontakt vorgenommen. Bitte laden Sie die Seite neu und nehmen Sie ihre Änderungen erneut vor."]
      return;
    }

    resp.json().then((result: any) => {
      const errors: Array<string> = [];
      Object.entries(result).forEach(entry => {
        errors.push(entry[0] + " " + entry[1]);
      });
      this.errors = errors;
    })
  }


  /**
   * Löscht eine Beziehung
   * @param relation Beziehung, die gelöscht werden soll
   */
  deleteRelation(relation: Relation) {
    this.personService.deleteRelation(relation.id);
    const indexToDelete = _.findIndex(this.person.relations, r => r.id === relation.id);
    this.person.relations.splice(indexToDelete, 1);
  }


  /**
   * Routet zur Startseite der Kontaktverwaltung
   */
  navigateToOverview() {
    this.router.navigate(['/kontakte']);
  }
}
