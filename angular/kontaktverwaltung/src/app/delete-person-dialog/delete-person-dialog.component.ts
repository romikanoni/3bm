import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PersonService } from '../person.service';
import { Router } from '@angular/router';
import { Person } from '../model/Person';

@Component({
  selector: 'delete-person-dialog',
  templateUrl: './delete-person-dialog.component.html',
  styleUrls: ['./delete-person-dialog.component.scss']
})
export class DeletePersonDialogComponent implements OnInit {

  @Input('person') person: Person;
  @Output('on-person-deleted') onPersonDeleted = new EventEmitter<boolean>();
  @Output('on-close') onClose = new EventEmitter<boolean>();


  constructor(private personService: PersonService, private router: Router) { }

  ngOnInit() {
  }

  deletePerson() {
    this.personService.deletePerson(this.person.id).then(() => {
      this.onPersonDeleted.emit();
      this.onClose.emit();
    });
  }

}
