import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Relation, Person } from '../model/Person';
import { PersonService } from '../person.service';
import { ActivatedRoute } from '@angular/router';
import { RelationTexts, RelationDefinition, RelationDefinitionBidirectional, RelationDefinitionUnidirectional } from '../model/RelationTexts';
import * as _ from 'lodash';

@Component({
  selector: 'relation',
  templateUrl: './relation.component.html',
  styleUrls: ['./relation.component.scss']
})
export class RelationComponent implements OnInit {

  public relation: Relation;

  persons: Array<Person>;
  @Input('primary-person') primaryPerson: Person;
  @Output('on-relation-created') onRelationCreated = new EventEmitter<boolean>();
  @Output('on-close') onClose = new EventEmitter<boolean>();
  secondaryPersonId: Number;
  relationTypeName: RelationDefinition;

  relationTypes: Array<RelationDefinition> = RelationTexts.getRelationDefinitions();
  relationType: RelationDefinition = null;


  btnChecked: boolean;

  constructor(private personService: PersonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.personService.fetchAll().then(pl => {
      this.persons = _.filter(pl, p => p.id !== this.primaryPerson.id);
    })
  }

  addRelation() {
    if(this.relationType.active){
      this.save(this.primaryPerson.id, this.secondaryPersonId, this.relationType);
    } else {
      this.save(this.secondaryPersonId, this.primaryPerson.id, this.relationType);
    }

  }

  save(primaryPersonId, secondaryPersonId, relationType){
    return this.personService.createRelation(primaryPersonId, secondaryPersonId, relationType.serverName).then(() => {
      this.onRelationCreated.emit();
      this.onClose.emit();
    });
  }
}