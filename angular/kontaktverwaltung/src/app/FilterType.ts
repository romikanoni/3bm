import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        return items.filter(it => {
            const objFields = Object.getOwnPropertyNames(it);
            const fieldValues = _.map(objFields, f => it[f])
            return _.filter(fieldValues, fv => fv && fv.toLowerCase && fv.toLowerCase().includes(searchText)).length > 0;
        });
    }
}