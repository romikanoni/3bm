import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KontaktlisteComponent } from './kontaktliste/kontaktliste.component';
import { KontaktDetailsComponent } from './kontakt-details/kontakt-details.component';
import { RelationComponent } from './relation/relation.component';

const routes: Routes = [
  { path: 'kontakte', component: KontaktlisteComponent },
  { path: 'kontakt/:id', component: KontaktDetailsComponent },
  { path: 'relation', component: RelationComponent },
  { path: '', redirectTo: '/kontakte', pathMatch: 'full' }
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
