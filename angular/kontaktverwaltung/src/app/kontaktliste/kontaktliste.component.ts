import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { Person } from '../model/Person';
import { Router } from '@angular/router';


@Component({
  selector: 'kontaktliste',
  templateUrl: './kontaktliste.component.html',
  styleUrls: ['./kontaktliste.component.scss']
})
export class KontaktlisteComponent implements OnInit {

  public searchText: string;
  persons: Array<Person>;
  personToDelete: number = null;

  constructor(private personService: PersonService, private router: Router) { }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.personService.fetchAll().then(pl => {
      this.persons = pl;
    })
  }

  onPersonClick(person: Person) {
    this.router.navigate(['kontakt', person.id]);
  }


  onCreatePerson(){
    this.router.navigate(['kontakt', "neu"]);
  }
}
