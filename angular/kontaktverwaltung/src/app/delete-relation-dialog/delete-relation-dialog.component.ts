import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Relation, Person } from '../model/Person';
import { PersonService } from '../person.service';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { NumberSymbol } from '@angular/common';
import { RelationTexts } from '../model/RelationTexts';

@Component({
  selector: 'delete-relation-dialog',
  templateUrl: './delete-relation-dialog.component.html',
  styleUrls: ['./delete-relation-dialog.component.scss']
})
export class DeleteRelationDialogComponent implements OnInit {

  @Input('relation') public relation: Relation;
  @Input('primary-person') public primaryPerson: Person;
  @Output('on-relation-deleted') onRelationDeleted = new EventEmitter<boolean>();
  @Output('on-close') onClose = new EventEmitter<boolean>();

  relatedPerson: Person;

  relationText: string;

  constructor(private personService: PersonService, private router: Router) { }

  ngOnInit() {
    this.relatedPerson = this.relation.getRelatedPerson(this.primaryPerson.id);
    this.relationText = RelationTexts.getRelationDefinition(this.relation.type, this.relation.isPrimaryPerson(this.primaryPerson.id)).getDescription();
  }

  deleteRelation() {
    this.personService.deleteRelation(this.relation.id).then(() => {
      this.onRelationDeleted.emit();
      this.onClose.emit();
    });
  }

}
