import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KontaktlisteComponent } from './kontaktliste/kontaktliste.component';
import { PersonService } from './person.service';
import { KontaktDetailsComponent } from './kontakt-details/kontakt-details.component';
import { RelationComponent } from './relation/relation.component';
import { FilterPipe } from './FilterType';
import { DeletePersonDialogComponent } from './delete-person-dialog/delete-person-dialog.component';
import { ClickStopPropagationDirective } from './click-stop-propagation.directive';
import { DeleteRelationDialogComponent } from './delete-relation-dialog/delete-relation-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    KontaktlisteComponent,
    KontaktDetailsComponent,
    RelationComponent,
    FilterPipe,
    DeletePersonDialogComponent,
    ClickStopPropagationDirective,
    DeleteRelationDialogComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [PersonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
