import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Person } from './model/Person';
import { RelationDefinition } from './model/RelationTexts';

/**
 * Server-URL angeben (Root)
 */
const baseUrl = 'http://localhost:8080';


@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor() { }

  /**
   * gibt alle Personen zurück
   */
  fetchAll() {
    return fetch(baseUrl + '/person')
      .then(response => response.json())
      .then((resp) => _.map(resp, (p) => new Person(p)));
  }

  /**
   * Gibt die Person mit angegebener ID zurück
   * @param id abzufragende ID der Person
   */
  fetchPerson(id: number): Promise<Person> {
    return fetch(baseUrl + '/person/' + id)
      .then(response => {
        return response.json().then((resp) => {
          return new Person(resp, response.headers.get("ETag"));
        });
      });
  }

  /**
   * Person speichern
   * @param person Person, die zu speichern ist
   */
  save(person: Person) {
    return fetch(baseUrl + '/person', {
      method: "POST",
      body: JSON.stringify(person),
      headers: {
        'Content-Type': 'application/json',
        'If-Match': person.etag,
      },
    })
  }

  /**
   * Neue Person erzeugen
   * @param person Person, die neu erstellt werden soll
   */
  create(person: Person) {
    return fetch(baseUrl + '/person', {
      method: "PUT",
      body: JSON.stringify(person),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  }

  /**
   * Löschen einer Person mit ID
   * @param id ID der zu löschenden Person
   */
  deletePerson(id) {
    return fetch(baseUrl + '/person/' + id, {
      method: "DELETE"
    });
  }

  /**
   * Erstellt eine neue Beziehung
   * @param person1Id ID der ersten Person
   * @param person2Id ID der zweiten Person
   * @param relationtypeName String der Beziehung
   */
  createRelation(person1Id: Number, person2Id: Number, relationtypeName: string) {
    return fetch(baseUrl + '/person/relation/' + person1Id + '/' + person2Id + '/' + relationtypeName, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
    })
  }


  /**
   * Beziehung zwischen zwei Personen anhand der Relation-ID löschen
   * @param relationId ID der zu löschenden Beziehung
   */
  deleteRelation(relationId) {
    return fetch(baseUrl + '/person/relation/' + relationId, {
      method: "DELETE"
    });
  }
}