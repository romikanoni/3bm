import * as _ from 'lodash';

export class Person {
    id: number
    vorname: string
    nachname: string
    anrede: string
    adresse: string
    telefon: string
    email: string
    handy: string
    relations: Array<Relation>
    etag: string

    constructor(p?: Person, etag?: string) {
        if (p) {
            this.id = p.id;
            this.vorname = p.vorname;
            this.nachname = p.nachname;
            this.anrede = p.anrede;
            this.adresse = p.adresse;
            this.telefon = p.telefon;
            this.email = p.email;
            this.handy = p.handy;
            this.relations = _.map(p.relations, rel => new Relation(rel));
            this.etag = etag;
        }
    }
}

export class Relation {
    id: number
    person1: Person
    person2: Person
    type: string

    constructor(r?: Relation) {
        if (r) {
            this.id = r.id;
            this.person1 = new Person(r.person1);
            this.person2 = new Person(r.person2);
            this.type = r.type;
        }
    }
    /**
     * gibt von einer bekannten Person die related Persons zurück
     * @param currentPersonId ID der zu überprüfenden Person
     */
    getRelatedPerson(currentPersonId: number) {
        if (this.person1.id === currentPersonId) {
            return this.person2;
        } else {
            return this.person1;
        }
    }
    /**
     * Überprüft ob die Person der aktive Part ist
     * @param personId 
     */
    isPrimaryPerson(personId: number) {
        return this.person1.id === personId;
    }
}