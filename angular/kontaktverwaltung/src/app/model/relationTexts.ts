import * as _ from 'lodash';

export class RelationDefinition {
    serverName: string;
    type?: Type;
    active?: boolean;

    shortName?: string;
    description?: string;
    shortNameActive?: string;
    shortNamePassiv?: string;
    descriptionActive?: string;
    descriptionPassiv?: string;

    getDescription?() {
        if (this.type === Type.UNIDIRECTIONAL) {
            return this.description;
        } else {
            return this.active ? this.descriptionActive : this.descriptionPassiv;
        }
    }

}

export class RelationDefinitionUnidirectional extends RelationDefinition {

    constructor(relationDefinitionUnidirectional: RelationDefinitionUnidirectional) {
        super();
        this.type = Type.UNIDIRECTIONAL;
        this.serverName = relationDefinitionUnidirectional.serverName;
        this.shortName = relationDefinitionUnidirectional.shortName;
        this.description = relationDefinitionUnidirectional.description;
    }
}

export class RelationDefinitionBidirectional extends RelationDefinition {
    
    constructor(relationDefinitionUnidirectional: RelationDefinitionBidirectional) {
        super();
        this.type = Type.BIDIRECTIONAL;
        this.serverName = relationDefinitionUnidirectional.serverName;
        this.shortNameActive = relationDefinitionUnidirectional.shortNameActive;
        this.shortNamePassiv = relationDefinitionUnidirectional.shortNamePassiv;
        this.descriptionActive = relationDefinitionUnidirectional.descriptionActive;
        this.descriptionPassiv = relationDefinitionUnidirectional.descriptionPassiv;
    }

}



enum Type {
    UNIDIRECTIONAL = 1,
    BIDIRECTIONAL
}


/**
 * Hier sind alle anzuzeigenden Texte von Beziehung angelegt.
 */
export class RelationTexts {

    public static getRelationDefinitions() {
        const results: Array<RelationDefinition> = [];
        const definitionKeys = Object.getOwnPropertyNames(RelationTexts)
        for (let entry of definitionKeys) {
            const definition = RelationTexts[entry];
            if (definition.type === Type.UNIDIRECTIONAL) {
                results.push(RelationTexts[entry]);
            } else if (definition.type === Type.BIDIRECTIONAL) {
                const activeRelation = _.clone(RelationTexts[entry]);
                activeRelation.active = true;

                const passiveRelation = _.clone(RelationTexts[entry]);
                passiveRelation.active = false;

                results.push(activeRelation, passiveRelation);
            }
        }
        return results;
    }

    public static getRelationDefinition(serverName: string, active: boolean) {
        const relation = RelationTexts[serverName]
        relation.active = active;
        return relation;
    }


    static VORGESETZTER = new RelationDefinitionBidirectional({
        serverName: 'VORGESETZTER',
        shortNameActive: 'Vorgesetzter',
        shortNamePassiv: 'Mitarbeiter',
        descriptionActive: 'ist Vorgesetzte/r von',
        descriptionPassiv: 'ist Mitarbeiter/in von'
    });

    static LEHRER = new RelationDefinitionBidirectional({
        serverName: 'LEHRER',
        shortNameActive: 'Lehrer',
        shortNamePassiv: 'Schüler',
        descriptionActive: 'ist Lehrer/in von',
        descriptionPassiv: 'ist Schüler/in von'
    });

    static FAMILIE = new RelationDefinitionUnidirectional({
        serverName: 'FAMILIE',
        shortName: 'Familie',
        description: 'gehört zur Familie von'
    });

    static BEKANNT = new RelationDefinitionUnidirectional({
        serverName: 'BEKANNT',
        shortName: 'Bekannter',
        description: 'ist ein/e Bekannte/r von'
    });

    static FREUNDE = new RelationDefinitionUnidirectional({
        serverName: 'FREUNDE',
        shortName: 'Freund',
        description: 'ist ein/e Freund/in von'
    });

    static AFFAERE = new RelationDefinitionUnidirectional({
        serverName: 'AFFAERE',
        shortName: 'Affaere',
        description: 'ist in einer Affäre mit'
    });
}
