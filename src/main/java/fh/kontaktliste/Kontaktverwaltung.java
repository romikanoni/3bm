package fh.kontaktliste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kontaktverwaltung {

    public static void main(String[] args) {
        SpringApplication.run(Kontaktverwaltung.class, args);
    }
}