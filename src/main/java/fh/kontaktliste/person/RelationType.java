package fh.kontaktliste.person;

import java.util.stream.Stream;

public enum RelationType {

    BEKANNT("bekannt", true),
    FREUNDE("freunde", true),
    FAMILIE("familie", true),
    AFFAERE("affaere", true),
    VORGESETZTER("vorgesetzter", false),
    LEHRER("lehrer", false);

    private String displayName;
    private boolean bidirectional;

    RelationType(String displayName, boolean bidirectional) {

        this.displayName = displayName;
        this.bidirectional = bidirectional;
    }

    /**
     * Gibt anhand eines Strings die Beziehungsart zurück
     * @param displayName Beziehungsart als String
     * @return Beziehungsart als RelationType
     */
    public static RelationType findByDisplayName(String displayName){
        return Stream.of(RelationType.values()).filter(r -> r.displayName.equals(displayName)).findAny().orElse(null);
    }

}
