package fh.kontaktliste.person;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long personId;

    @NotBlank(message = "Vorname ist erforderlich")
    @Pattern(regexp = "[a-zA-Z ]*")
    private String vorname;

    @NotBlank(message = "Nachname ist erforderlich")
    @Pattern(regexp = "[a-zA-Z ]*")
    private String nachname;

    @NotBlank(message = "Anrede ist erforderlich")
    @Pattern(regexp = "[a-zA-Z ]*[. ]*[a-zA-Z ]*[. ]*[a-zA-Z ]*[. ]*")
    private String anrede;

    @NotBlank(message = "Adresse ist erforderlich")
    @Pattern(regexp = "[a-zA-Z0-9-äöüß ]+[, ]{2}[0-9 ]{5}[a-zA-Zäöüß ]+")
    private String adresse;

    @NotBlank(message = "Telefonnummer ist erforderlich")
    @Pattern(regexp = "[0-9+/ ]*")
    private String telefon;

    @NotBlank(message = "Email-Adresse ist erforderlich")
    @Email(message = "Email-Adresse ist nicht gültig")
    private String email;

    @NotBlank(message = "Handynummer ist erforderlich")
    @Pattern(regexp = "[0-9+/ ]*")
    private String handy;


    @OneToMany(mappedBy = "person1", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<PersonRelation> outgoingRelations = new HashSet<>();

    @OneToMany(mappedBy = "person2", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<PersonRelation> incomingRelations = new HashSet<>();

    public Person() {
    }

    public Person(long personId, String vorname, String nachname, String anrede, String adresse, String telefon,
                  String email, String handy) {
        this.personId = personId;
        this.vorname = vorname;
        this.nachname = nachname;
        this.anrede = anrede;
        this.adresse = adresse;
        this.telefon = telefon;
        this.email = email;
        this.handy = handy;
    }


    public Long getId() {
        return personId;
    }

    public void setId(long id) {
        this.personId = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    /**
     * Gibt alle Beziehungen zu einer Person zurück
     * @return Set of Relations
     */
    public Set<PersonRelation> getRelations() {
        Set<PersonRelation> relations = new HashSet<>();
        relations.addAll(incomingRelations);
        relations.addAll(outgoingRelations);
        return relations;
    }

    public void setIncomingRelations(Set<PersonRelation> incomingRelations) {
        this.incomingRelations = incomingRelations;
    }

    public void setOutgoingRelations(Set<PersonRelation> outgoingRelations) {
        this.outgoingRelations = outgoingRelations;
    }

    public Set<PersonRelation> getOutgoingRelations() {
        return outgoingRelations;
    }

    public Set<PersonRelation> getIncomingRelations() {
        return incomingRelations;
    }


    @Override
    public int hashCode() {
        int result = personId.hashCode();
        result = 31 * result + (vorname != null ? vorname.hashCode() : 0);
        result = 31 * result + (nachname != null ? nachname.hashCode() : 0);
        result = 31 * result + (anrede != null ? anrede.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        result = 31 * result + (telefon != null ? telefon.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (handy != null ? handy.hashCode() : 0);
        result = 31 * result + (outgoingRelations != null ? outgoingRelations.hashCode() : 0);
        result = 31 * result + (incomingRelations != null ? incomingRelations.hashCode() : 0);
        return result;
    }
}
