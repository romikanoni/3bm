package fh.kontaktliste.person;

import fh.kontaktliste.ResourceNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@RestController
public class PersonController {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    PersonService personService;

    /**
     * Gibt alle angelegten Kontakte zurück.
     * @return Liste von Personen
     */
    @RequestMapping(method = RequestMethod.GET, value = "/person")
    public List<Person> getPersonList() {
        logger.info("Abfrage aller Personen");
        return personService.getPersonList();
    }

    /**
     * Zeigt den Kontakt mit der angegeben ID an
     * @param id ID der abzufragenden Person
     * @return angelegten Kontakt
     */
    @RequestMapping(method = RequestMethod.GET, value = "/person/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable long id) {
        logger.info(String.format("Abfrage der Person mit ID %d", id));
        Person person = personService.getPerson(id);
        if (person == null) {
            throw new ResourceNotFoundException();
        }
        ResponseEntity<Person> responseEntity = ResponseEntity.ok()
                .eTag(String.valueOf(person.hashCode()))
                .body(person);

        return responseEntity;
    }

    /**
     * Kontakt mit PUT erstellen Beispiel:
     * @param person anzulegende Person
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/person")
    public Person addPerson(@Valid @RequestBody Person person) {
        if (person.getId() != null && personService.getPerson(person.getId()) != null) {
            throw new IllegalStateException(String.format("Person with id: %d already exists", person.getId()));
        }
        Person savedPerson = personService.addPerson(person);
        logger.info(String.format("Neue Person angelegt. ID: %d", savedPerson.getId()));
        return savedPerson;
    }

    /**
     * Kontakt ändern
     * @param person neuer Inhalt des Kontakts
     * @return Wenn ein Fehler vorliegt, wird der Fehler zurückgegeben, ansonsten die aktualisierte Person inklusive ETag-Header
     */
    @RequestMapping(method = RequestMethod.POST, value = "/person")
    public ResponseEntity<Person> updatePerson(@Valid @RequestBody Person person, @RequestHeader HttpHeaders headers) {
        if (person.getId() == null) {
            logger.error("Keine ID angegeben");
            throw new IllegalStateException("Id is not specified");
        }
        Person existingPerson = personService.getPerson(person.getId());
        if (existingPerson == null) {
            logger.error(String.format("Person mit ID existiert nicht. ID: %d", person.getId()));
            throw new IllegalStateException(String.format("Person with id: %d does not exists", person.getId()));
        }
        List<String> ifMatch = headers.getIfMatch();
        if (ifMatch.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        String preUpdateHash = String.valueOf(existingPerson.hashCode());
        if (ifMatch.stream().noneMatch(m -> m.substring(1, m.length() - 1).equals(preUpdateHash))) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).build();
        }
        try {
            logger.info("Person geändert.");
            Person personAnswer = personService.updatePerson(person);

            return ResponseEntity.ok()
                    .eTag(String.valueOf(person.hashCode()))
                    .body(personAnswer);

        } catch (OptimisticLockingFailureException ex) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    /**
     * Kontakt mit entsprechender ID löschen
     * @param id ID des Kontakts
     * @return NO-CONTENT HTTP-Status
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/person/{id}")
    public ResponseEntity deletePerson(@PathVariable long id) {
        personService.deletePerson(id);
        logger.info("Person gelöscht");
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    /**
     * Neue Beziehung anlegen anhand von zwei IDs
     * @param id1  ID Person 1
     * @param id2  ID Person 2
     * @param type Beziehungstyp
     * @return angelegte Person
     */
    @RequestMapping(method = RequestMethod.POST, value = "/person/relation/{id1}/{id2}/{type}")
    public PersonRelation addRelation(@Valid @PathVariable long id1, @PathVariable long id2, @PathVariable String type) {
        logger.info("Neue Beziehung angelegt");
        PersonRelation personRelation = personService.addRelation(id1, id2, type);
        System.out.println(personRelation.toString());
        return personRelation;
    }


    /**
     * Löscht eine Beziehung anhand der relation-id.
     * @param id ID der Beziehung
     * @return NO-CONTENT HTTP-Status
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/person/relation/{id}")
    public ResponseEntity deleteRelation(@PathVariable long id) {
        personService.deleteRelation(id);
        logger.info("Beziehung gelöscht");
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


    /**
     * Gibt bei dem HTTP-Status Bad-Request die Fehlermeldungen zurück
     * @param ex Fehler
     * @return alle Fehlermeldungen
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
