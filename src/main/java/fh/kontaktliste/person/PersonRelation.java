package fh.kontaktliste.person;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class PersonRelation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "Person 1 ist erforderlich")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person1_id")
    @JsonIgnoreProperties("relations")
    private Person person1;

    @NotNull(message = "Person 2 ist erforderlich")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person2_id")
    @JsonIgnoreProperties("relations")
    private Person person2;

    @NotNull(message = "Beziehungsart ist erforderlich")
    @Column(name = "type")
    private RelationType type;

    private PersonRelation() {}

    public PersonRelation(Person p1, Person p2, RelationType type) {
        this.person1 = p1;
        this.person2 = p2;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson1() {
        return person1;
    }

    public void setPerson1(Person person1) {
        this.person1 = person1;
    }

    public Person getPerson2() {
        return person2;
    }

    public void setPerson2(Person person2) {
        this.person2 = person2;
    }

    public RelationType getType() {
        return type;
    }

    public void setType(RelationType type) {
        this.type = type;
    }


    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + person1.getId().intValue();
        result = 31 * result + person2.getId().intValue();
        result = 31 * result + type.hashCode();
        return result;
    }
}
