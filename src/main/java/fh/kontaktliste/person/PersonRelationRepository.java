package fh.kontaktliste.person;

import org.springframework.data.repository.CrudRepository;

public interface PersonRelationRepository extends CrudRepository<PersonRelation, Long> {

}
