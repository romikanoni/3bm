package fh.kontaktliste.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PersonRelationRepository personRelationRepository;

    public List<Person> getPersonList() {
        return StreamSupport.stream(personRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public Person getPerson(long id) {
        return personRepository.findById(id).orElse(null);
    }

    public Person addPerson(Person person) {
        return personRepository.save(person);
    }

    public Person updatePerson(Person person) {
        Person existingPerson = personRepository.findById(person.getId()).get();
        existingPerson.setAdresse(person.getAdresse());
        existingPerson.setAnrede(person.getAnrede());
        existingPerson.setEmail(person.getEmail());
        existingPerson.setHandy(person.getHandy());
        existingPerson.setNachname(person.getNachname());
        existingPerson.setVorname(person.getVorname());
        existingPerson.setTelefon(person.getTelefon());

        return personRepository.save(existingPerson);
    }

    public void deletePerson(long id) {
        personRepository.deleteById(id);
    }


    /**
     * Erstellt eine neue Beziehung zwischen zwei Kontakten
     * @param id1 ID der ersten Person
     * @param id2 ID der zweiten Person
     * @param type Beziehungsart als String
     * @return die angelegte Beziehung
     */
    public PersonRelation addRelation(long id1, long id2, String type) {
        Person person1 = personRepository.findById(id1).get();
        Person person2 = personRepository.findById(id2).get();
        RelationType relationType = RelationType.findByDisplayName(type) == null ? RelationType.valueOf(type.toUpperCase()) : RelationType.findByDisplayName(type);
        PersonRelation personRelation = new PersonRelation(person1, person2, relationType);
        return personRelationRepository.save(personRelation);
    }

    /**
     * Löscht eine Beziehung
     * @param id ID der zu löschenden Beziehung
     */
    public void deleteRelation(long id) {
        PersonRelation relation = personRelationRepository.findById(id).get();
        personRelationRepository.delete(relation);
    }
}
