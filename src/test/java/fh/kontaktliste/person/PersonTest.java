package fh.kontaktliste.person;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {

    private Person person;
    private PersonRelation incomingRelation;
    private PersonRelation outgoingRelation;


    @BeforeEach
    void setUp() {
        person = new Person();
        Set<PersonRelation> incomingRelations = person.getIncomingRelations();
        incomingRelation = new PersonRelation(person, null, RelationType.BEKANNT);
        incomingRelations.add(incomingRelation);

        Set<PersonRelation> outgoingRelations = person.getOutgoingRelations();
        outgoingRelation = new PersonRelation(null, person, RelationType.BEKANNT);
        outgoingRelations.add(outgoingRelation);

    }

    @Test
    void getRelations() {
        Set<PersonRelation> relations = person.getRelations();
        assertThat(relations).contains(incomingRelation, outgoingRelation);
    }
}