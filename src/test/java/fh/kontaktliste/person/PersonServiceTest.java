package fh.kontaktliste.person;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SpringBootTest
class PersonServiceTest {

    @Mock
    PersonRepository personRepository;

    @Mock
    PersonRelationRepository relationRepository;

    @InjectMocks
    PersonService personService;

    @Test
    void updatePerson() {
        Person oldPerson = new Person(1, "Max", "Mustermann", "Herr",
                "Musterstraße 1, 70000 Stuttgart", "0711 / 123456",
                "roman.glorim@test.de", "0999 123456");

        Person person = new Person(1, "Roman", "Glorim", "Herr",
                "Musterstraße 1, 70000 Stuttgart", "0711 / 123456",
                "roman.glorim@test.de", "0999 123456");

        when(personRepository.findById(1L)).thenReturn(Optional.of(oldPerson));
        when(personRepository.save(any())).thenAnswer((Answer<Person>) inv -> inv.getArgument(0));
        Person updatedPerson = personService.updatePerson(person);
        assertThat(updatedPerson).isEqualToComparingFieldByField(person);
    }

    @Test
    void addRelation() {
        int relationId = 5;
        int personId1 = 1;
        int personId2 = 2;

        when(personRepository.findById(any()))
                .thenAnswer((Answer<Optional<Person>>) i -> Optional.of(new Person() {{
                    setId(i.getArgument(0));
                }}));

        when(relationRepository.save(any())).thenAnswer((Answer<PersonRelation>) inv -> {
            PersonRelation personRelationToSave = inv.getArgument(0);
            personRelationToSave.setId(relationId);
            return personRelationToSave;
        });

        PersonRelation returnedPersonRelation = personService.addRelation(personId1, personId2, "bekannt");

        assertThat(returnedPersonRelation.getPerson1().getId()).isEqualTo(personId1);
        assertThat(returnedPersonRelation.getPerson2().getId()).isEqualTo(personId2);
        assertThat(returnedPersonRelation.getType()).isEqualTo(RelationType.BEKANNT);
        assertThat(returnedPersonRelation.getId()).isEqualTo(relationId);

        ArgumentCaptor<PersonRelation> savedPersonRelationCaptor = ArgumentCaptor.forClass(PersonRelation.class);
        verify(relationRepository).save(savedPersonRelationCaptor.capture());
        PersonRelation savedPersonRelation = savedPersonRelationCaptor.getValue();
        assertThat(savedPersonRelation).isEqualToComparingFieldByField(returnedPersonRelation);

    }
}